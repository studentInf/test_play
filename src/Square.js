import React, { Component } from 'react';

class Square extends Component {

  render() {
    var button = <div className="square" onClick={() => this.props.onClick()}></div>;
    if(this.props.value === 1){
      button = <div className="square" onClick={() => this.props.onClick()}><img src={require('../public/img/img-1.png')} alt="X"/></div>
    }else if(this.props.value === -1){
      button = <div className="square" onClick={() => this.props.onClick()}><img src={require('../public/img/img1.png')} alt="0"/></div>
    }
    return (
        button
    );

  }
}

export default Square;
