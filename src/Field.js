import React, { Component } from 'react';
import axios from 'axios';

import Square from './Square';

class Field extends Component {

  constructor(props){

      super(props);
      this.state = {
          squares: Array(9).fill(0),
          isActiveX: true,
      }

  }

  componentDidMount() {
      axios.get('http://606ep.ru:8080')
          .then(function(response){
          let data = response.data;
          let fullArr = [];
         for(let i = 0; i < data.length; i++){
             fullArr = fullArr.concat(data[i]);
         }

          this.setState({
              squares: fullArr,
          });

          }.bind(this));
    }

  changeView(i){
      const squares = this.state.squares.slice();
      if(squares[i] !== 0) return;
      squares[i] = this.state.isActiveX ? -1 : 1;
      this.setState({
          squares: squares,
          isActiveX: !this.state.isActiveX
      });
  }

  renderSquare(i){
      return <Square value={this.state.squares[i]} onClick={() => this.changeView(i)}/>;
  }

  render() {
    return (
        <div>
            <div className="row">
                {this.renderSquare(0)}
                {this.renderSquare(1)}
                {this.renderSquare(2)}
            </div>
            <div className="row">
                {this.renderSquare(3)}
                {this.renderSquare(4)}
                {this.renderSquare(5)}
            </div>
            <div className="row">
                {this.renderSquare(6)}
                {this.renderSquare(7)}
                {this.renderSquare(8)}
            </div>
        </div>
    );
  }
}

export default Field;
