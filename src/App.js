import React, { Component } from 'react';

import Field from './Field';
// import Ring from './Ring';
// import Player from './Player';
// import Square from './Square';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="play">
        <Field/>
      </div>
    );
  }
}

export default App;
